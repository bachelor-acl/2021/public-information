---
order: 6
---

# Powershell

### Rouge

{% highlight powershell %}

Get-ChildItem | Select-Object -First 3

{% endhighlight %}

### Markdown

```powershell
Get-ChildItem | Select-Object -First 3
```

# C

### Rouge

{% highlight c %}
#include <stdio.h>

int main() {
    printf("Hello, World!");
    return 0;
}
{% endhighlight %}

### Markdown

```c
#include <stdio.h>

int main() {
    printf("Hello, World!");
    return 0;
}
```

# C++

{% highlight cpp %}

#include <iostream>
int main() {
    std::cout << "Hello, World!" << endl;
    return 0;
}
{% endhighlight %}

# Java

{% highlight java %}
class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello, World!"); 
    }
}
{% endhighlight %}